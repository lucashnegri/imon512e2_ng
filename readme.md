# About

Provides a device driver and library for using the I-MON spectrometer with GNU / Linux systems.
This is a modification of the driver originally provided by Alexander V. Lyasin [1].

Works with Linux v 3.10+ (tested with versions 3.12.17 and 3.14.13).

# Usage

To compile the kernel module and load it (requires kernel headers):

    $ cd module
    $ make
    
Install:
    
    # insmod imon512e2.ko
    # depmod -a
    $ modprobe imon512e2
    
Testing:

    $ cat /proc/imon512e2
    
Using the spectrometer:

    $ cd library
    $ make
    # ./example
    
Acessing the device requires special permissions. To use it as a simple user, you can add a udev rule like
the following:
    KERNEL=="imon512e22", MODE="0666", OPTIONS+="last_rule"
    
# License

Same as the original [1] one (GPL / BSD).

[1]: http://sourceforge.net/projects/spectrometerdri/
