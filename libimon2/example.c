#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stropts.h>
#include <string.h>
#include <arpa/inet.h>

#include "libimon2.h"

const char*  driver_name    = "/dev/imon512e22";
const double exposure_cycle = 60e3;     // 60 us
const double exposure_time  = 50e3;     // 50 us
const short  capture_mode   = 0x1;      // continuous mode?
const short  startcaptmode  = 0x0100;   // ??

int main()
{
    /* open the device */
    int fd = open(driver_name, O_RDONLY);
    if(fd < 0)
    {
        perror("open error: ");
        return 0;
    }
    
    /* set the capture mode */
    if (imon2_set_capturemode(fd, htons(capture_mode))) { printf("error for imon2_set_capturemode\n"); goto exit; }
    
    /* configure the exposure cycle and time */
    {
        double temp_double;
        
        over64((char *)&temp_double, (char *)&exposure_cycle);
        if (imon2_set_exposurecycle(fd, temp_double)) { printf("error for imon2_set_exposurecycle\n"); goto exit; }
        
        over64( (char*) &temp_double, (char*) &exposure_time);
        if (imon2_set_exposuretime(fd, temp_double)) { printf("error for imon2_set_exposuretime\n"); goto exit; }
    }

    /* data acquisition */
    {
        unsigned short buffer[512];
        char temp[0x30];
        
        if (imon2_startcapture(fd, htons(startcaptmode))) { printf("error for start_capture\n");     goto exit; }
        if (imon2_begin_cycle(fd, temp))                  { printf("error for imon2_begin_cycle\n"); goto exit; }
        
        /* read the header */
        if( read(fd, (char*)buffer, 0x200) < 0 ) { printf("error for read header\n"); goto exit; }
        
        /* read the data */
        if( read(fd, (char*)buffer, 0x400) < 0 ) { printf("error for read header\n"); goto exit; }
        
        /* print the data */
        int i;
        for(i = 0; i < 512; ++i)
            printf("%d\n", buffer[511-i]);
        
        if (imon2_end_cycle(fd, temp)) { printf("error for end cycle\n"); goto exit; }
        if (imon2_stopcapture(fd)) { printf("error for stop capture"); goto exit; }
    }
    
    /* clean-up */
    {
        char temp[0x30];
        imon2_begin_cycle(fd, temp);
        imon2_end_header(fd, temp);
        imon2_end_cycle(fd, temp);
    }
    
exit:
    close(fd);
    
    return 0;
}

