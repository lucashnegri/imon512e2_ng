/*
 * imon512e2.h
 * Original author: Alexander Lyasin <alexander.lyasin@gmail.com>
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/kref.h>
#include <linux/proc_fs.h>
#include <linux/uaccess.h>
#include <linux/usb.h>
#include <linux/mutex.h>
#include <linux/string.h>
#include <linux/time.h>

/*
 * Version Information
 */
#define DRIVER_VERSION "1.01"
/*
* Driver description
*/
#define DRIVER_DESC "I-MON 512E-USB2 Driver"
/*
 * Module Autor
 */
#define DRIVER_AUTOR "Alexander Lyasin <alexander.lyasin@gmail.com>"
/*
* Vendor & product ids
*/
#define HAMAMATSU_VID 0x0661
#define HAMAMATSU_PID 0x2930

#define DRIVER_NAME "imon512e2%d"
#define DRIVER_NAME_P "imon512e2"
#define IMON_MINOR_BASE 0x10

#define IMONERR(format, args...) printk(KERN_ERR "imon512e2 err: " format "\n", ##args )
#define IMONINF(format, args...) printk(KERN_INFO "imon512e2 info: " format "\n", ##args)

#define MAX_TRANSFER		(PAGE_SIZE - 512)
#define USB_SET_BULK_TIMEOUT	1000
#define BUFFER_IN_SIZE		0x400
#define BUFFER_OUT_SIZE		0x40
#define MAX_ENDPOINT_NUM	0x2
#define COMMAND_LENGTH		0x8

#define GET_MODULE_INFORMATION		0x01
#define GET_SPECTRO_INFORMATION		0x02
#define GET_MODULE_PROPERTY		0x03
#define GET_DATAPOSITION_PROPERTY	0x04
#define GET_DATACOUNT_PROPERTY		0x05
#define GET_DATATRANSMIT_PROPERTY	0x06
#define GET_DATATRIGGEROFFSET_PROPERTY	0x07
#define GET_EXPOSURE_PROPERTY		0x08
#define GET_GAIN_PROPERTY		0x09
#define GET_ADOFFSET_PROPERTY		0x0A
#define GET_COOLING_PROPERTY		0x0B
#define GET_WAVELENGTH_PROPERTY		0x0C
#define GET_IMAGE_SIZE			0x0D
#define GET_MODULEREADOUT_TIME		0x0E
#define GET_CAPTURE_MODE		0x0F
#define SET_CAPTURE_MODE		0x10
#define GET_DATA_POSITION		0x11
#define SET_DATA_POSITION		0x12
#define GET_DATA_COUNT			0x13
#define SET_DATA_COUNT			0x14
#define GET_DATA_TRANSMIT		0x15
#define SET_DATA_TRANSMIT		0x16
#define GET_DATATRIGGER_OFFSET		0x17
#define SET_DATATRIGGER_OFFSET		0x18
#define GET_EXPOSURE_TIME		0x19
#define SET_EXPOSURE_TIME		0x1A
#define GET_EXPOSURE_CYCLE		0x1B
#define SET_EXPOSURE_CYCLE		0x1C
#define GET_TRIGGER_MODE		0x1D /* begin supposed methods */
#define SET_TRIGGER_MODE		0x1E
#define GET_TRIGGER_POLARITY		0x1F
#define SET_TRIGGER_POLARITY		0x20
#define GET_TRIGGER_OUTPUT		0x21
#define SET_TRIGGER_OUTPUT		0x22
#define GET_BINNING_SUBARRAY		0x23 /* begin unimplemented methods */
#define SET_BINNING_SUBARRAY		0x24 /* end unimlpemented methods */ /* end supposed methods */
#define GET_GAIN			0x25
#define	SET_GAIN			0x26 
#define GET_ADOFFSET			0x27 /* begin supposed methods */
#define SET_ADOFFSET			0x28
#define GET_CALIBRATION_COEFFICIENT	0x29
#define GET_COOLING_TEMPERATURE		0x2A
#define SET_COOLING_TEMPERATURE		0x2B
#define GET_COOLING_STATUS		0x2C
#define SET_COOLING_STATUS		0x2D
#define GET_CURRENT_TEMPERATURE		0x2E /* end supposed methods */
#define CAPTURE_START			0x2F
#define GET_CAPTURE_STATUS		0x30
#define CAPTURE_STOP			0x31

#define GET_BEGCYCLE_TRANSFER		0x41
#define GET_ENDCYCLE_TRANSFER		0x42
#define GET_ENDCAPTURE_TRANSFER		0x43
#define GET_ENDHEADER_TRANSFER		0x44

#define RCV_BEGINCYCLE_PKT		{0x02, 0x80}
#define RCV_ENDCYCLE_PKT		{0x04, 0x80, 0x00, 0x00, 0x00, 0x00}
#define RCV_ENDCAPTURE_PKT		{0x01, 0x80, 0x00, 0x00, 0x00, 0x00}
#define RCV_HEADER_SIZE			0x200
#define RCV_IMAGE_SIZE			0x400

#define IMON512E2_MAGIC_IOCTL		0xbd
/*
 * ioctl commands
 */
#define IO_GET_MODULE_INFORMATION		_IOR(IMON512E2_MAGIC_IOCTL, GET_MODULE_INFORMATION, void *)
#define IO_GET_SPECTRO_INFORMATION		_IOR(IMON512E2_MAGIC_IOCTL, GET_SPECTRO_INFORMATION, void *)
#define IO_GET_MODULE_PROPERTY			_IOR(IMON512E2_MAGIC_IOCTL, GET_MODULE_PROPERTY, void *)
#define IO_GET_DATAPOSITION_PROPERTY		_IOR(IMON512E2_MAGIC_IOCTL, GET_DATAPOSITION_PROPERTY, void *)
#define IO_GET_DATACOUNT_PROPERTY		_IOR(IMON512E2_MAGIC_IOCTL, GET_DATACOUNT_PROPERTY, __s32)
#define IO_GET_DATATRANSMIT_PROPERTY		_IOR(IMON512E2_MAGIC_IOCTL, GET_DATATRANSMIT_PROPERTY, __s32)
#define IO_GET_DATATRIGGEROFFSET_PROPERTY	_IOR(IMON512E2_MAGIC_IOCTL, GET_DATATRIGGEROFFSET_PROPERTY, void *)
#define IO_GET_EXPOSURE_PROPERTY		_IOR(IMON512E2_MAGIC_IOCTL, GET_EXPOSURE_PROPERTY, void *)
#define IO_GET_GAIN_PROPERTY			_IOR(IMON512E2_MAGIC_IOCTL, GET_GAIN_PROPERTY, __s32)
#define IO_GET_ADOFFSET_PROPERTY		_IOR(IMON512E2_MAGIC_IOCTL, GET_ADOFFSET_PROPERTY, void *)
#define IO_GET_COOLING_PROPERTY			_IOR(IMON512E2_MAGIC_IOCTL, GET_COOLING_PROPERTY, void *)
#define IO_GET_WAVELENGTH_PROPERTY		_IOR(IMON512E2_MAGIC_IOCTL, GET_WAVELENGTH_PROPERTY, void *)
#define IO_GET_IMAGE_SIZE			_IOR(IMON512E2_MAGIC_IOCTL, GET_IMAGE_SIZE, void *)
#define IO_GET_MODULEREADOUT_TIME		_IOR(IMON512E2_MAGIC_IOCTL, GET_MODULEREADOUT_TIME, void *)
#define IO_GET_CAPTURE_MODE			_IOR(IMON512E2_MAGIC_IOCTL, GET_CAPTURE_MODE, __s32)
#define IO_SET_CAPTURE_MODE			_IOW(IMON512E2_MAGIC_IOCTL, SET_CAPTURE_MODE, __s16)
#define IO_GET_DATA_POSITION			_IOR(IMON512E2_MAGIC_IOCTL, GET_DATA_POSITION, __s32)
#define IO_SET_DATA_POSITION			_IOW(IMON512E2_MAGIC_IOCTL, SET_DATA_POSITION, __s32)
#define IO_GET_DATA_COUNT			_IOR(IMON512E2_MAGIC_IOCTL, GET_DATA_COUNT, __s32)
#define IO_SET_DATA_COUNT			_IOW(IMON512E2_MAGIC_IOCTL, SET_DATA_COUNT, __s32)
#define IO_GET_DATA_TRANSMIT			_IOR(IMON512E2_MAGIC_IOCTL, GET_DATA_TRANSMIT, __s32)
#define IO_SET_DATA_TRANSMIT			_IOW(IMON512E2_MAGIC_IOCTL, SET_DATA_TRANSMIT, __s32)
#define IO_GET_DATATRIGGER_OFFSET		_IOR(IMON512E2_MAGIC_IOCTL, GET_DATATRIGGER_OFFSET, __s32)
#define IO_SET_DATATRIGGER_OFFSET		_IOW(IMON512E2_MAGIC_IOCTL, SET_DATATRIGGER_OFFSET, __s32)
#define IO_GET_EXPOSURE_TIME			_IOR(IMON512E2_MAGIC_IOCTL, GET_EXPOSURE_TIME, __s64)
#define IO_SET_EXPOSURE_TIME			_IOW(IMON512E2_MAGIC_IOCTL, SET_EXPOSURE_TIME, __s64)
#define IO_GET_EXPOSURE_CYCLE			_IOR(IMON512E2_MAGIC_IOCTL, GET_EXPOSURE_CYCLE, __s64)
#define IO_SET_EXPOSURE_CYCLE			_IOW(IMON512E2_MAGIC_IOCTL, SET_EXPOSURE_CYCLE, __s64)
/* begin supposed methods */
#define IO_GET_TRIGGER_MODE			_IOR(IMON512E2_MAGIC_IOCTL, GET_TRIGGER_MODE, __s32) 
#define IO_SET_TRIGGER_MODE			_IOW(IMON512E2_MAGIC_IOCTL, SET_TRIGGER_MODE, __s32)
#define IO_GET_TRIGGER_POLARITY			_IOR(IMON512E2_MAGIC_IOCTL, GET_TRIGGER_POLARITY, __s32)
#define IO_SET_TRIGGER_POLARITY			_IOW(IMON512E2_MAGIC_IOCTL, SET_TRIGGER_POLARITY, __s32)
#define IO_GET_TRIGGER_OUTPUT			_IOR(IMON512E2_MAGIC_IOCTL, GET_TRIGGER_OUTPUT, __s32)
#define IO_SET_TRIGGER_OUTPUT			_IOW(IMON512E2_MAGIC_IOCTL, SET_TRIGGER_OUTPUT, __s32)
/* begin unimplemented methods */
#define IO_GET_BINNING_SUBARRAY			
#define IO_SET_BINNING_SUBARRAY
/* end unimlpemented methods */ 
/* end supposed methods */
#define IO_GET_GAIN				_IOR(IMON512E2_MAGIC_IOCTL, GET_GAIN, __s32)
#define	IO_SET_GAIN				_IOW(IMON512E2_MAGIC_IOCTL, SET_GAIN, void *)
 /* begin supposed methods */
#define IO_GET_ADOFFSET				_IOR(IMON512E2_MAGIC_IOCTL, GET_ADOFFSET, __s32)
#define IO_SET_ADOFFSET				_IOW(IMON512E2_MAGIC_IOCTL, SET_ADOFFSET, __s32)
#define IO_GET_CALIBRATION_COEFFICIENT		_IOR(IMON512E2_MAGIC_IOCTL, GET_CALIBRATION_COEFFICIENT, void *)
#define IO_GET_COOLING_TEMPERATURE		_IOR(IMON512E2_MAGIC_IOCTL, GET_COOLING_TEMPERATURE, __s64)
#define IO_SET_COOLING_TEMPERATURE		_IOW(IMON512E2_MAGIC_IOCTL, SET_COOLING_TEMPERATURE, __s64)
#define IO_GET_COOLING_STATUS			_IOR(IMON512E2_MAGIC_IOCTL, GET_COOLING_STATUS, __s32)
#define IO_SET_COOLING_STATUS			_IOW(IMON512E2_MAGIC_IOCTL, SET_COOLING_STATUS, __s32)
#define IO_GET_CURRENT_TEMPERATURE		_IOR(IMON512E2_MAGIC_IOCTL, GET_CURRENT_TEMPERATURE, __s64)
 /* end supposed methods */
#define IO_CAPTURE_START			_IOW(IMON512E2_MAGIC_IOCTL, CAPTURE_START, __s16)
#define IO_GET_CAPTURE_STATUS			_IOR(IMON512E2_MAGIC_IOCTL, GET_CAPTURE_STATUS, void *)
#define IO_CAPTURE_STOP				_IO(IMON512E2_MAGIC_IOCTL, CAPTURE_STOP)
/* additional (own) commands */
#define IO_GET_BEGCYCLE_TRANSFER		_IOR(IMON512E2_MAGIC_IOCTL, GET_BEGCYCLE_TRANSFER, void *)
#define IO_GET_ENDCYCLE_TRANSFER		_IOR(IMON512E2_MAGIC_IOCTL, GET_ENDCYCLE_TRANSFER, void *)
#define IO_GET_ENDCAPTURE_TRANSFER		_IOR(IMON512E2_MAGIC_IOCTL, GET_ENDCAPTURE_TRANSFER, void *)
#define IO_GET_ENDHEADER_TRANSFER		_IOR(IMON512E2_MAGIC_IOCTL, GET_ENDHEADER_TRANSFER, void *)
/*
 * end ioctl commands
 */
/*
 * #define IMON512E2_DEBUG
 */
struct stuff_usb_imon512e {
	struct usb_device       *stuff_usb_dev;
	struct usb_interface    *stuff_interface;
	unsigned char           *stuff_bulk_in_buffer;
	unsigned char		*stuff_bulk_out_buffer;
	size_t			stuff_bulk_in_size;
	size_t			stuff_bulk_out_size;
	size_t			stuff_rest_bulk_in_buffer;
	__u8			stuff_bulk_in_endpointAddr[MAX_ENDPOINT_NUM];
	__u8			stuff_bulk_out_endpointAddr[MAX_ENDPOINT_NUM];
	size_t			stuff_bulk_in_endpointAddr_size;
	size_t			stuff_bulk_out_endpointAddr_size;
	struct kref		stuff_kref;
	struct mutex		stuff_io_mutex;
};

static struct usb_device_id id_table [] = {
	{ USB_DEVICE(HAMAMATSU_VID, HAMAMATSU_PID) }, 	
	{ }				
};

#ifdef IMON512E2_DEBUG
#define DBG(format, args...)	printk(KERN_DEBUG "imon512e2 dbg: " format "\n", ##args)
#else
#define DBG(format, args...)	do{}while(0)
#endif

#define RCV_CHECK(retval)	if (retval) { \
					IMONERR("error for rcv_reply from %s with retval: %i", __FUNCTION__, retval); \
					return retval; \
				}

#define SND_CHECK(retval)	if (retval) { \
					IMONERR("error for snd_command from %s with retval %i", __FUNCTION__, retval); \
					return retval; \
				}

#define CHK_CPY_TO_USER(retval, count_bytes, dstbuf, srcbuf)	if (!retval) { \
       									if (copy_to_user(dstbuf, srcbuf, count_bytes)) { \
										IMONERR("error for copy to user space, count_bytes: %i", \
												count_bytes); \
										retval=-ENOMEM; \
										goto exit; \
									} \
								}

#define CHK_AFTER_SND(retval)					if (retval) { \
									IMONERR("error from %s with errno %d", \
											__FUNCTION__, retval);  \
									goto exit; \
								}
#define CHK_CPY_FROM_USER(dstbuf, userbuf, count_bytes)		if (copy_from_user(dstbuf, userbuf, count_bytes)) { \
									IMONERR("error for copy from user space, count_bytes: %lu", \
											count_bytes); \
									retval=-ENOMEM; \
									goto exit; \
								}

