/*
 * I-MON E-USB 2.0 spectrometer USB driver
 *
 * Copyright (C) 2008 Alexander Lyasin <alexander.lyasin@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation (either version 2 of the License, or
 * (at your option) any later version) or under the terms of the
 * BSD License.
 *
 * Last modification: 2014.05.16 - lhn
 * 
 */
#include "imon512e2.h"

//#define IMON512E2_DEBUG

static struct usb_device_id generic_device_ids[2];
static struct proc_dir_entry *imon_proc_dir_entry;
static struct file_operations proc_file_ops;
static struct usb_driver imon512e_driver;
static int dev_minor=0;

MODULE_DEVICE_TABLE(usb, id_table);

static void delete_stuff_usb_imon512e (struct kref *kref) {
    struct stuff_usb_imon512e *dev=container_of(kref, struct stuff_usb_imon512e, stuff_kref);
    usb_put_dev(dev->stuff_usb_dev);
    kfree(dev->stuff_bulk_in_buffer);
    kfree(dev->stuff_bulk_out_buffer);
    kfree(dev);
}

#ifdef IMON512E2_DEBUG
static int debug_out_buffer (const char *buf, size_t buflen) {
    int i;
    for (i=0; i<buflen; i++) {
        DBG("buf[%i]=%x", i, (unsigned char)buf[i]);
    }
    return 0;
}
#endif

static inline int send_command_pipe (struct stuff_usb_imon512e *dev, 
                    unsigned pipe,     
                    __s32 command, 
                    __u32 buflen, 
                    const char *buf, 
                    int *count_bytes) {
    __s32 c_param;
    __u32 b_param;
    unsigned packetlen=sizeof(command)+sizeof(buflen)+buflen;
    if (dev->stuff_bulk_out_size<packetlen) return -ENOMEM;
    c_param=htonl(command);
    memcpy(dev->stuff_bulk_out_buffer, (void *)&(c_param), sizeof(command));
    b_param=htonl(buflen);
    memcpy(dev->stuff_bulk_out_buffer+sizeof(command), (void *)&(b_param), sizeof(buflen));
    if (buflen) memcpy(dev->stuff_bulk_out_buffer+sizeof(command)+sizeof(buflen), buf, buflen);
    return usb_bulk_msg(dev->stuff_usb_dev, pipe, dev->stuff_bulk_out_buffer, 
            buflen+sizeof(command)+sizeof(buflen), count_bytes, USB_SET_BULK_TIMEOUT);
    
}

static inline int rcv_reply_pipe (struct stuff_usb_imon512e *dev, 
                unsigned pipe, 
                __u32 msglen, 
                int *count_bytes) {
    if (dev->stuff_bulk_in_size<msglen) return -ENOMEM;
    return usb_bulk_msg(dev->stuff_usb_dev, pipe, dev->stuff_bulk_in_buffer, msglen,
            count_bytes, USB_SET_BULK_TIMEOUT);
}

static inline int send_command0 (struct stuff_usb_imon512e *dev, 
            __s32 command, 
            __u32 buflen, 
            const char *buf, 
            int *count_sendbytes) {
    unsigned sndbulk_pipe;
    sndbulk_pipe=usb_sndbulkpipe(dev->stuff_usb_dev, dev->stuff_bulk_out_endpointAddr[0]);
    return send_command_pipe(dev, sndbulk_pipe, command, buflen, buf, count_sendbytes);
}

static inline int rcv_reply0 (struct stuff_usb_imon512e *dev, __u32 msglen, int *count_rcvbytes) {
    unsigned rcvbulk_pipe;
    rcvbulk_pipe=usb_rcvbulkpipe(dev->stuff_usb_dev, dev->stuff_bulk_in_endpointAddr[0]);
    return rcv_reply_pipe(dev, rcvbulk_pipe, msglen, count_rcvbytes);
}

static inline int send_command1 (struct stuff_usb_imon512e *dev, 
            __s32 command, 
            __u32 buflen, 
            const char *buf, 
            int *count_sendbytes) {
    unsigned sndbulk_pipe;
    sndbulk_pipe=usb_sndbulkpipe(dev->stuff_usb_dev, dev->stuff_bulk_out_endpointAddr[1]);
    return send_command_pipe(dev, sndbulk_pipe, command, buflen, buf, count_sendbytes);
}

static inline int rcv_reply1 (struct stuff_usb_imon512e *dev, __u32 msglen, int *count_rcvbytes) {
    unsigned rcvbulk_pipe;
    rcvbulk_pipe=usb_rcvbulkpipe(dev->stuff_usb_dev, dev->stuff_bulk_in_endpointAddr[1]);
    return rcv_reply_pipe(dev, rcvbulk_pipe, msglen, count_rcvbytes);
}

static inline int sndcmd_get_dataXXX (struct stuff_usb_imon512e *dev, 
            __s32 cmd,  
            __u32 reply_buflen, 
            int *count_bytes) {
    int retval=0;
    retval=send_command0(dev, cmd, 0x0, NULL, count_bytes);
    SND_CHECK(retval);
    retval=rcv_reply0(dev, reply_buflen, count_bytes);
    RCV_CHECK(retval);
    return retval;
}

static inline int sndcmd_set_dataXXX (struct stuff_usb_imon512e *dev,
            __s32 cmd,
            const char *buf,
            size_t buflen,
            __u32 reply_buflen,
            int *count_bytes) {
    int retval=0;
    retval=send_command0(dev, cmd, buflen, buf, count_bytes);
    SND_CHECK(retval);
    retval=rcv_reply0(dev, reply_buflen, count_bytes);
    RCV_CHECK(retval);
    return retval;
}

static inline int rcv_begincycle_transfer (struct stuff_usb_imon512e *dev, int *count_bytes) {
    char ans[]=RCV_BEGINCYCLE_PKT;
    return rcv_reply0(dev, sizeof(ans), count_bytes);
}

static inline int rcv_endcycle_transfer (struct stuff_usb_imon512e *dev, int *count_bytes) {
    char ans[]=RCV_ENDCYCLE_PKT;
    return rcv_reply0(dev, sizeof(ans), count_bytes);
}

static inline int rcv_endcapture_transfer (struct stuff_usb_imon512e *dev, int *count_bytes) {
    char ans[]=RCV_ENDCAPTURE_PKT;
    return rcv_reply0(dev, sizeof(ans), count_bytes);
}

static inline int rcv_header_transfer (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return rcv_reply1(dev, RCV_HEADER_SIZE, count_bytes);
}

static inline int rcv_image_transfer (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return rcv_reply1(dev, RCV_IMAGE_SIZE, count_bytes);
}

static inline int sndcmd_capture_start (struct stuff_usb_imon512e *dev, __s16 mode, int *count_bytes) {
    return sndcmd_set_dataXXX(dev, CAPTURE_START, (const char*)&mode, sizeof(mode), 0x2, count_bytes); 
}

static inline int sndcmd_capture_stop (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return send_command0(dev, CAPTURE_STOP, 0x0, NULL, count_bytes);
}

static inline int sndcmd_get_datatransmit (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_DATA_TRANSMIT, 0x4, count_bytes);
}

static inline int sndcmd_set_datatransmit (struct stuff_usb_imon512e *dev, __s32 data, int *count_bytes) {
    return sndcmd_set_dataXXX(dev, SET_DATA_TRANSMIT, (const char*)&data, sizeof(data), 0x4, count_bytes);
}

static inline int sndcmd_get_dataposition (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_DATA_POSITION, 0x4, count_bytes);
}

static inline int sndcmd_set_dataposition (struct stuff_usb_imon512e *dev, __s32 data, int *count_bytes) {
    return sndcmd_set_dataXXX(dev, SET_DATA_POSITION, (const char*)&data, sizeof(data), 0x4, count_bytes);
}

static inline int sndcmd_get_exposurecycle (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_EXPOSURE_CYCLE, 0x8, count_bytes);
}

static inline int sndcmd_set_exposurecycle (struct stuff_usb_imon512e *dev, __s64 data, int *count_bytes) {
    return sndcmd_set_dataXXX(dev, SET_EXPOSURE_CYCLE, (const char*)&data, sizeof(data), 0x8, count_bytes);
}

static inline int sndcmd_get_exposuretime (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_EXPOSURE_TIME, 0x8, count_bytes);
}

static inline int sndcmd_set_exposuretime (struct stuff_usb_imon512e *dev, __s64 data, int *count_bytes) {
    return sndcmd_set_dataXXX(dev, SET_EXPOSURE_TIME, (const char*)&data, sizeof(data), 0x8, count_bytes);
}

static inline int sndcmd_get_capturemode (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_CAPTURE_MODE, 0x4, count_bytes);
}

static inline int sndcmd_set_capturemode (struct stuff_usb_imon512e *dev, __s16 mode, int *count_bytes) {
    return sndcmd_set_dataXXX(dev, SET_CAPTURE_MODE, (const char*)&mode, sizeof(mode), 0x2, count_bytes);
}

static inline int sndcmd_get_moduleinformation (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_MODULE_INFORMATION, 0x80, count_bytes);
}

static inline int sndcmd_get_spectroinformation (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_SPECTRO_INFORMATION, 0x18, count_bytes);
}

static inline int sndcmd_get_moduleproperty (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_MODULE_PROPERTY, 0x18, count_bytes);
}

static inline int sndcmd_get_datapositionproperty (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_DATAPOSITION_PROPERTY, 0x8, count_bytes);
}

static inline int sndcmd_get_datacountproperty (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_DATACOUNT_PROPERTY, 0x4, count_bytes);
}

static inline int sndcmd_get_datatransmitproperty (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_DATATRANSMIT_PROPERTY, 0x4, count_bytes);
}

static inline int sndcmd_get_datatriggeroffsetproperty (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_DATATRIGGEROFFSET_PROPERTY, 0x8, count_bytes);
}

static inline int sndcmd_get_exposureproperty (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_EXPOSURE_PROPERTY, 0x30, count_bytes);
}

static inline int sndcmd_get_gainproperty (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_GAIN_PROPERTY, 0x4, count_bytes);
}

static inline int sndcmd_get_adoffsetproperty (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_ADOFFSET_PROPERTY, 0x8, count_bytes);
}

static inline int sndcmd_get_coolingproperty (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_COOLING_PROPERTY, 0x18, count_bytes);
}

static inline int sndcmd_get_wavelengthproperty (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_WAVELENGTH_PROPERTY, 0x8, count_bytes);
}

static inline int sndcmd_get_imagesize (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_IMAGE_SIZE, 0x8, count_bytes);
}

static inline int sndcmd_get_modulereadouttime (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_MODULEREADOUT_TIME, 0x8, count_bytes);
}

static inline int sndcmd_get_datacount (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_DATA_COUNT, 0x4, count_bytes);
}

static inline int sndcmd_set_datacount (struct stuff_usb_imon512e *dev, __s32 data, int *count_bytes) {
    return sndcmd_set_dataXXX(dev, SET_DATA_COUNT, (const char*)&data, sizeof(data), 0x4, count_bytes);
}

static inline int sndcmd_get_datatriggeroffset (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_DATATRIGGER_OFFSET, 0x4, count_bytes);
}

static inline int sndcmd_set_datatriggeroffset (struct stuff_usb_imon512e *dev, __s32 data, int *count_bytes) {
    return sndcmd_set_dataXXX(dev, SET_DATATRIGGER_OFFSET, (const char*)&data, sizeof(data), 0x4, count_bytes);
}

static inline int sndcmd_get_triggermode (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_TRIGGER_MODE, 0x4, count_bytes);
}

static inline int sndcmd_set_triggermode (struct stuff_usb_imon512e *dev, __s32 data, int *count_bytes) {
    return sndcmd_set_dataXXX(dev, SET_TRIGGER_MODE, (const char*)&data, sizeof(data), 0x4, count_bytes);
}

static inline int sndcmd_get_triggerpolarity (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_TRIGGER_POLARITY, 0x4, count_bytes);
}

static inline int sndcmd_set_triggerpolarity (struct stuff_usb_imon512e *dev, __s32 data, int *count_bytes) {
    return sndcmd_set_dataXXX(dev, SET_TRIGGER_POLARITY, (const char*)&data, sizeof(data), 0x4, count_bytes);
}

static inline int sndcmd_get_triggeroutput (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_TRIGGER_OUTPUT, 0x4, count_bytes);
}

static inline int sndcmd_set_triggeroutput (struct stuff_usb_imon512e *dev, __s32 data, int *count_bytes) {
    return sndcmd_set_dataXXX(dev, SET_TRIGGER_OUTPUT, (const char*)&data, sizeof(data), 0x4, count_bytes);
}

static inline int sndcmd_get_gain (struct stuff_usb_imon512e *dev, __s32 data, int *count_bytes) {
    return sndcmd_set_dataXXX(dev, GET_GAIN, (const char*)&data, sizeof(data), 0x4, count_bytes);
}

static inline int sndcmd_set_gain (struct stuff_usb_imon512e *dev, __s64 data, int *count_bytes) {
    return sndcmd_set_dataXXX(dev, SET_GAIN, (const char*)&data, sizeof(data), 0x4, count_bytes);
}

static inline int sndcmd_get_adoffset (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_ADOFFSET, 0x4, count_bytes);
}

static inline int sndcmd_set_adoffset (struct stuff_usb_imon512e *dev, __s32 data, int *count_bytes) {
    return sndcmd_set_dataXXX(dev, SET_ADOFFSET, (const char*)&data, sizeof(data), 0x4, count_bytes);
}

static inline int sndcmd_get_calibrationcoefs (struct stuff_usb_imon512e *dev, __s32 data, int *count_bytes) {
    return sndcmd_set_dataXXX(dev, GET_CALIBRATION_COEFFICIENT, (const char*)&data, sizeof(data), 0x30, count_bytes);
}

static inline int sndcmd_get_coolingtemperature (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_COOLING_TEMPERATURE, 0x8, count_bytes);
}

static inline int sndcmd_set_coolingtemperature (struct stuff_usb_imon512e *dev, __s64 data, int *count_bytes) {
    return sndcmd_set_dataXXX(dev, SET_COOLING_TEMPERATURE, (const char*)&data, sizeof(data), 0x8, count_bytes);
}

static inline int sndcmd_get_coolingstatus (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_COOLING_STATUS, 0x4, count_bytes);
}

static inline int sndcmd_set_coolingstatus (struct stuff_usb_imon512e *dev, __s32 data, int *count_bytes) {
    return sndcmd_set_dataXXX(dev, SET_COOLING_STATUS, (const char*)&data, sizeof(data), 0x4, count_bytes);
}

static inline int sndcmd_get_currenttemperature (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_CURRENT_TEMPERATURE, 0x8, count_bytes);
}

static inline int sndcmd_get_capturestatus (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return sndcmd_get_dataXXX(dev, GET_CAPTURE_STATUS, 0x8, count_bytes);
}

static inline int sndcmd_get_endheader (struct stuff_usb_imon512e *dev, int *count_bytes) {
    return rcv_header_transfer(dev, count_bytes);
}    

static int create_proc_node (void) {
    int retval=-ENOMEM;
    imon_proc_dir_entry = proc_create(DRIVER_NAME_P, 0, NULL, &proc_file_ops);

    if (!imon_proc_dir_entry) {
        IMONERR("error: can't initialize /proc/" DRIVER_NAME_P);
        return retval;
    }
    
    return 0;
}

static void remove_proc_node (void) {
    remove_proc_entry(DRIVER_NAME_P, NULL);
}
/*
 * opens file /proc/imon512e%d for writing and reading
 */
static int proc_open  (struct inode *inode, struct file *file) {
    int retval=0;
    struct stuff_usb_imon512e *dev=NULL;
    struct usb_interface *interface;
    unsigned minor=dev_minor;
    
    interface=usb_find_interface(&imon512e_driver, minor);
    if (!interface) {
        IMONERR("can't find interface from %s minor %d", __FUNCTION__, minor);
        retval=-ENODEV;
        goto exit;
    }
    
    dev=usb_get_intfdata(interface);
    if (!dev) {
        IMONERR("can't get data driver's from %s", __FUNCTION__);
        retval=-ENODEV;
        goto exit;
    }
    
    kref_get(&dev->stuff_kref);
    mutex_lock(&dev->stuff_io_mutex);
    usb_autopm_get_interface(interface);
    file->private_data=dev;
    
exit:
    mutex_unlock(&dev->stuff_io_mutex);
    return retval;
}
/*
 * reads from /proc/imon512%d
 */
static ssize_t proc_read (struct file *file, char __user *buffer, size_t count, loff_t *ppos) {
    int retval=0;
    static char was_readed=0;
    struct stuff_usb_imon512e *dev;
    int count_bytes=0;
    char proc_to[0x80];

    if (was_readed) {
        was_readed=0;
        return 0;
    }
    DBG("begin of proc_read");
    memset(proc_to, 0, sizeof(proc_to));
    dev=(struct stuff_usb_imon512e*)file->private_data;
    if (!dev) {
        IMONERR("can't get data driver's from %s", __FUNCTION__);
        return -ENODEV;
    }
    mutex_lock(&dev->stuff_io_mutex);
    if (!dev->stuff_interface) {
        retval=-ENODEV;
        IMONERR("can't get interface from %s", __FUNCTION__);
        goto exit;
    }
    retval=sndcmd_get_moduleinformation(dev, &count_bytes);
    if (retval) goto exit;
    if (sprintf(proc_to, "vendor: %s\nproduct: %s\nserial: %s\nfirmware: %s\n",
            dev->stuff_bulk_in_buffer,
            dev->stuff_bulk_in_buffer+32,
            dev->stuff_bulk_in_buffer+64,
            dev->stuff_bulk_in_buffer+96)<0) {
        IMONERR("error copying from buffer, retval: %d", retval);
        goto exit;
    }
    if (copy_to_user(buffer, proc_to, sizeof(proc_to))) {
        retval=-EFAULT;
        IMONERR("error for copy to user space: %s", __FUNCTION__);
    } else {
        retval=count_bytes;
    }
    was_readed=1;
exit:
    mutex_unlock(&dev->stuff_io_mutex);
      return retval;
}
/*
 * writes in /proc/imon512e%d
 */
static ssize_t proc_write (struct file *file, const char __user *buffer, size_t count, loff_t *ppos) {
    IMONERR("operation is not support for procfs");        
    return -EINVAL;
}
/*
 * close operation for procfs file
 */
static int proc_release (struct inode *inode, struct file *file) {
    struct stuff_usb_imon512e *dev=(struct stuff_usb_imon512e*)file->private_data;
    if (!dev) {
        IMONERR("can't release device from procfs");
        return -ENODEV;
    }
    mutex_lock(&dev->stuff_io_mutex);
    if (dev->stuff_interface) usb_autopm_put_interface(dev->stuff_interface);
    mutex_unlock(&dev->stuff_io_mutex);
    kref_put(&dev->stuff_kref, delete_stuff_usb_imon512e);
    return 0;
}

static int imon512e_open (struct inode *inode, struct file *file) {
    int retval=0;
    struct stuff_usb_imon512e *dev;
    struct usb_interface *interface;
    unsigned minor;
    
    minor=iminor(inode);
    interface=usb_find_interface(&imon512e_driver, minor);
    if (!interface) {
        IMONERR("can't find interface from %s minor %d", __FUNCTION__, minor);
        retval=-ENODEV;
        goto exit;
    }
    
    dev=usb_get_intfdata(interface);
    if (!dev) {
        IMONERR("can't get data driver's from %s", __FUNCTION__);
        retval=-ENODEV;
        goto exit;
    }
    kref_get(&dev->stuff_kref);
    
    usb_autopm_get_interface(interface);
    dev->stuff_rest_bulk_in_buffer=0;
    file->private_data=dev;
    
exit:
    return retval;
}

static int imon512e_release (struct inode *inode, struct file *file) {
    struct stuff_usb_imon512e *dev=(struct stuff_usb_imon512e*)file->private_data;
    if (!dev) {
        IMONERR("can't release device from devfs");
        return -ENODEV;
    }
    mutex_lock(&dev->stuff_io_mutex);
    if (dev->stuff_interface) usb_autopm_put_interface(dev->stuff_interface);
    mutex_unlock(&dev->stuff_io_mutex);
    kref_put(&dev->stuff_kref, delete_stuff_usb_imon512e);
    return 0;
}

static ssize_t imon512e_write (struct file *file, const char __user *buffer, size_t count, loff_t *ppos) {
    IMONERR("write operation is not supported");
    return -EINVAL;
}

static ssize_t imon512e_read (struct file *file, char __user *buffer, size_t buf_size, loff_t *ppos) {
    int retval=0, 
        count_bytes, 
        buf_offset=0, 
        count_copy=0;
    struct stuff_usb_imon512e *dev=(struct stuff_usb_imon512e *)file->private_data;

    if (!dev) {
        IMONERR("can't driver data from %s", __FUNCTION__);
        return -ENODEV;
    }
    if (buf_size<RCV_HEADER_SIZE) {
        IMONERR("too few buffer space for read operation");
        return -ENOSPC;
    }
    mutex_lock(&dev->stuff_io_mutex);
    if (!dev->stuff_interface || !dev->stuff_usb_dev) {
        IMONERR("no device");
        retval=-ENODEV;
        goto exit;
    }
    while (buf_size-buf_offset) {
        if (RCV_HEADER_SIZE<=buf_size && buf_size<RCV_IMAGE_SIZE) {
            if ((retval=rcv_header_transfer(dev, &count_bytes))) {
                IMONERR("read header error with errno: %i", retval);
                goto exit;
            }
            count_copy=buf_offset?(buf_size-buf_offset>RCV_HEADER_SIZE?
                count_bytes:buf_size-buf_offset):count_bytes;
            if (copy_to_user(buffer+buf_offset, dev->stuff_bulk_in_buffer, count_copy)) {
                retval=-ENOMEM;
                goto exit;
            }
        } else {
            if ((retval=rcv_image_transfer(dev, &count_bytes))) {
                IMONERR("read image error with errno: %i", retval);
                goto exit;
            }
            count_copy=buf_offset?(buf_size-buf_offset>RCV_IMAGE_SIZE?
                count_bytes:buf_size-buf_offset):count_bytes;
            if (copy_to_user(buffer+buf_offset, dev->stuff_bulk_in_buffer, count_copy)) {
                retval=-ENOMEM;
                goto exit;
            }
            
        }    
        buf_offset+=count_copy;
    }
    retval=buf_offset;
exit:
    mutex_unlock(&dev->stuff_io_mutex);
    return retval;
}    

static long imon512e_ioctl (/* struct inode *inode,*/struct file *file, unsigned cmd, unsigned long param) {
    int retval=0, count_bytes;
    __s16 setval16;
    __s32 setval32;
    __s64 setval64;
    struct stuff_usb_imon512e *dev=(struct stuff_usb_imon512e*)file->private_data;
    void __user *buffer=(void __user *)param;
    if (!dev) {
        IMONERR("can't get driver data from %s", __FUNCTION__);
        return -ENODEV;
    }
    mutex_lock(&dev->stuff_io_mutex);
    if (!dev->stuff_interface) {
        retval=-ENODEV;
        IMONERR("can't get driver interface from %s", __FUNCTION__);
        goto exit;
    }
    setval16=(__s16)param;
    setval32=(__s32)param;
    switch(cmd) {
        case IO_GET_MODULE_INFORMATION:
            retval=sndcmd_get_moduleinformation(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_SPECTRO_INFORMATION:
            retval=sndcmd_get_spectroinformation(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_MODULE_PROPERTY:
            retval=sndcmd_get_moduleproperty(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_DATAPOSITION_PROPERTY:
            retval=sndcmd_get_datapositionproperty(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_DATACOUNT_PROPERTY:
            retval=sndcmd_get_datacountproperty(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_DATATRANSMIT_PROPERTY:
            retval=sndcmd_get_datatransmitproperty(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_DATATRIGGEROFFSET_PROPERTY:
            retval=sndcmd_get_datatriggeroffsetproperty(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_EXPOSURE_PROPERTY:
            retval=sndcmd_get_exposureproperty(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_GAIN_PROPERTY:
            retval=sndcmd_get_gainproperty(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_ADOFFSET_PROPERTY:
            retval=sndcmd_get_adoffsetproperty(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_COOLING_PROPERTY:
            retval=sndcmd_get_coolingproperty(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_WAVELENGTH_PROPERTY:
            retval=sndcmd_get_wavelengthproperty(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_IMAGE_SIZE:
            retval=sndcmd_get_imagesize(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_MODULEREADOUT_TIME:
            retval=sndcmd_get_modulereadouttime(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_CAPTURE_MODE:
            retval=sndcmd_get_capturemode(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_SET_CAPTURE_MODE:
            retval=sndcmd_set_capturemode(dev, setval16, &count_bytes);
            CHK_AFTER_SND(retval);
            DBG("set_capture_mode buffer: %0x", *(__s16*)dev->stuff_bulk_in_buffer);            
            break;
        case IO_GET_DATA_POSITION:
            sndcmd_get_dataposition(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_SET_DATA_POSITION:
            retval=sndcmd_set_dataposition(dev, setval32, &count_bytes);
            CHK_AFTER_SND(retval);
            DBG("set_data_position buffer: %0x", *(__s32*)dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_DATA_COUNT:
            retval=sndcmd_get_datacount(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_SET_DATA_COUNT:
            retval=sndcmd_set_datacount(dev, setval32, &count_bytes);
            CHK_AFTER_SND(retval);
            DBG("set_data_count buffer: %0x", *(__s32*)dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_DATA_TRANSMIT:
            retval=sndcmd_get_datatransmit(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_SET_DATA_TRANSMIT:
            retval=sndcmd_set_datatransmit(dev, setval32, &count_bytes);
            CHK_AFTER_SND(retval);
            DBG("set_data_transmit buffer: %0x", *(__s32*)dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_DATATRIGGER_OFFSET:
            retval=sndcmd_get_datatriggeroffset(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_SET_DATATRIGGER_OFFSET:
            retval=sndcmd_set_datatriggeroffset(dev, setval32, &count_bytes);
            CHK_AFTER_SND(retval);
            DBG("set_datatrigger_offset buffer: %0x", *(__s32*)dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_EXPOSURE_TIME:
            retval=sndcmd_get_exposuretime(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_SET_EXPOSURE_TIME:
            CHK_CPY_FROM_USER((void *)&setval64, buffer, sizeof(setval64));
            retval=sndcmd_set_exposuretime(dev, setval64, &count_bytes);
            CHK_AFTER_SND(retval);
            DBG("set_exposure_time buffer: %0x", *(__s32*)dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_EXPOSURE_CYCLE:
            retval=sndcmd_get_exposurecycle(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_SET_EXPOSURE_CYCLE:
            CHK_CPY_FROM_USER((void *)&setval64, buffer, sizeof(setval64));
            retval=sndcmd_set_exposurecycle(dev, setval64, &count_bytes);    
            CHK_AFTER_SND(retval);
            DBG("set_exposure_cycle buffer: %0x", *(__s32*)dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_TRIGGER_MODE:
            retval=sndcmd_get_triggermode(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_SET_TRIGGER_MODE:
            retval=sndcmd_set_triggermode(dev, setval32, &count_bytes);
            CHK_AFTER_SND(retval);
            DBG("set_trigger_mode buffer: %0x", *(__s32*)dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_TRIGGER_POLARITY:
            retval=sndcmd_get_triggerpolarity(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_SET_TRIGGER_POLARITY:
            retval=sndcmd_set_triggerpolarity(dev, setval32, &count_bytes);
            CHK_AFTER_SND(retval);
            DBG("set_trigger_polarity buffer: %0x", *(__s32*)dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_TRIGGER_OUTPUT:
            retval=sndcmd_get_triggeroutput(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_SET_TRIGGER_OUTPUT:
            retval=sndcmd_set_triggeroutput(dev, setval32, &count_bytes);
            CHK_AFTER_SND(retval);
            DBG("set_trigger_output buffer: %0x", *(__s32*)dev->stuff_bulk_in_buffer);
            break;
/********************************************
 * undefined operations
        case IO_GET_BINNING_SUBARRAY:
            break;
        case IO_SET_BINNING_SUBARRAY:
            break;
 *
 ********************************************/
        case IO_GET_GAIN:
            CHK_CPY_FROM_USER(&setval32, buffer, sizeof(setval32));
            retval=sndcmd_get_gain(dev, setval32, &count_bytes); 
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_SET_GAIN:
            CHK_CPY_FROM_USER(&setval64, buffer, sizeof(setval64));
            retval=sndcmd_set_gain(dev, setval64, &count_bytes);
            CHK_AFTER_SND(retval);
            DBG("set_gain buffer: %0x", *(__s32*)dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_ADOFFSET:
            retval=sndcmd_get_adoffset(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_SET_ADOFFSET:
            retval=sndcmd_set_adoffset(dev, setval32, &count_bytes);
            CHK_AFTER_SND(retval);
            DBG("set_adoffset buffer: %0x", *(__s32*)dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_CALIBRATION_COEFFICIENT:
            CHK_CPY_FROM_USER(&setval32, buffer, sizeof(setval32));
            retval=sndcmd_get_calibrationcoefs(dev, setval32, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_COOLING_TEMPERATURE:
            retval=sndcmd_get_coolingtemperature(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_SET_COOLING_TEMPERATURE:
            CHK_CPY_FROM_USER(&setval64, buffer, sizeof(setval64));
            retval=sndcmd_set_coolingtemperature(dev, setval64, &count_bytes);
            CHK_AFTER_SND(retval);
            DBG("set_cooling_temperature buffer: %0x", *(__s32*)dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_COOLING_STATUS:
            retval=sndcmd_get_coolingstatus(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_SET_COOLING_STATUS:
            retval=sndcmd_set_coolingstatus(dev, setval32, &count_bytes);
            CHK_AFTER_SND(retval);
            DBG("set_cooling_status buffer: %0x", *(__s32*)dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_CURRENT_TEMPERATURE:
            retval=sndcmd_get_currenttemperature(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_CAPTURE_START:
            retval=sndcmd_capture_start(dev, setval16, &count_bytes);
            CHK_AFTER_SND(retval);
            DBG("capture_start buffer: %0x", *(__s16*)dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_CAPTURE_STATUS:
            retval=sndcmd_get_capturestatus(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_CAPTURE_STOP:
            retval=sndcmd_capture_stop(dev, &count_bytes);
            CHK_AFTER_SND(retval);
            break;
        case IO_GET_BEGCYCLE_TRANSFER:
            retval=rcv_begincycle_transfer(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_ENDCYCLE_TRANSFER:
            retval=rcv_endcycle_transfer(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_ENDCAPTURE_TRANSFER:
            retval=rcv_endcapture_transfer(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        case IO_GET_ENDHEADER_TRANSFER:
            retval=sndcmd_get_endheader(dev, &count_bytes);
            CHK_CPY_TO_USER(retval, count_bytes, buffer, dev->stuff_bulk_in_buffer);
            break;
        default:
            retval=-EINVAL;
            goto exit;
    }
        
exit:
    mutex_unlock(&dev->stuff_io_mutex);
    return retval;
}

static struct file_operations imon512e_fops = {
    owner:        THIS_MODULE,
    open:        imon512e_open,
    release:    imon512e_release,
    write:        imon512e_write,
    read:        imon512e_read,
    unlocked_ioctl:    imon512e_ioctl
};

static struct usb_class_driver imon512e_class_driver = {
    .name =        DRIVER_NAME,
    .fops =        &imon512e_fops,
    .minor_base =    IMON_MINOR_BASE    
};

static int imon512e_probe (struct usb_interface *interface, const struct usb_device_id *device_id) {
    int retval=-ENOMEM;
    int i;
    const struct usb_device_id *id_pattern;
    struct stuff_usb_imon512e *dev=NULL;
    struct usb_host_interface *uhi_current_setting;
    struct usb_endpoint_descriptor *endpoint;

    id_pattern=usb_match_id(interface, generic_device_ids);
    if (id_pattern) {
        retval=-EEXIST;
        IMONERR("match found for this device!\n");
        goto fault;
    }
    dev=kzalloc(sizeof(*dev), GFP_KERNEL);
    if (!dev) {
        IMONERR("Out of memory: can't create device struct");
        goto fault;
    }
    dev->stuff_bulk_in_size=BUFFER_IN_SIZE;
    dev->stuff_bulk_out_size=BUFFER_OUT_SIZE;
    dev->stuff_bulk_in_buffer=kmalloc(dev->stuff_bulk_in_size, GFP_KERNEL);
    if (!dev->stuff_bulk_in_buffer) {
        IMONERR("Out of memory: can't allocate IN buffer");
        goto fault;
     }
    dev->stuff_bulk_out_buffer=kmalloc(dev->stuff_bulk_out_size, GFP_KERNEL);
    if (!dev->stuff_bulk_out_buffer) {
        IMONERR("Out of memory: can't allocate OUT buffer");
        goto fault;
    }
    kref_init(&dev->stuff_kref);
    mutex_init(&dev->stuff_io_mutex);
    dev->stuff_usb_dev=usb_get_dev(interface_to_usbdev(interface));
    dev->stuff_interface=interface;
    uhi_current_setting = interface->cur_altsetting;
    IMONINF("number endpoints: %d", uhi_current_setting->desc.bNumEndpoints);
    for (i = 0; i < uhi_current_setting->desc.bNumEndpoints; ++i) {
        endpoint = &uhi_current_setting->endpoint[i].desc;
#ifdef IMON512E2_DEBUG
        if (endpoint) {
            IMONINF("USB endpoint descriptor:\n\
                    bLength: %d\n\
                    bDescriptorType: %d\n\
                    bEndpointAddress: %d\n\
                    bmAttributes: %d\n\
                    wMaxPacketSize: %d\n\
                    bInterval: %d\n\
                    bSynchAddress: %d\n", 
                    endpoint->bLength,
                    endpoint->bDescriptorType,
                    endpoint->bEndpointAddress,
                    endpoint->bmAttributes,
                    endpoint->wMaxPacketSize,
                    endpoint->bInterval,
                    endpoint->bSynchAddress);
        } else {
            IMONINF("USB endpoint descriptor is NULL");
        }
        if (!usb_endpoint_is_bulk_in(endpoint)) {
            IMONINF("usb_endpoint_is_bulk_in NULL");
        }
        if (!usb_endpoint_is_bulk_out(endpoint)) {
            IMONINF("usb_endpoint_is_bulk_out NULL");
        }
#endif
        if (usb_endpoint_is_bulk_in(endpoint) && 
                dev->stuff_bulk_in_endpointAddr_size<MAX_ENDPOINT_NUM) {
            dev->stuff_bulk_in_endpointAddr[dev->stuff_bulk_in_endpointAddr_size++]=
                endpoint->bEndpointAddress;
        } else if (usb_endpoint_is_bulk_out(endpoint) 
                && dev->stuff_bulk_out_endpointAddr_size<MAX_ENDPOINT_NUM) {
            dev->stuff_bulk_out_endpointAddr[dev->stuff_bulk_out_endpointAddr_size++]=
                endpoint->bEndpointAddress;
        } else {
            IMONERR("can't create bulk-in or bulk-out endpoints, undefined endpoint");
            if (endpoint) {
                IMONERR("USB endpoint descriptor:\n\
                    bLength: %d\n\
                    bDescriptorType: %d\n\
                    bEndpointAddress: %d\n\
                    bmAttributes: %d\n\
                    wMaxPacketSize: %d\n\
                    bInterval: %d\n\
                    bSynchAddress: %d\n", 
                    endpoint->bLength,
                    endpoint->bDescriptorType,
                    endpoint->bEndpointAddress,
                    endpoint->bmAttributes,
                    endpoint->wMaxPacketSize,
                    endpoint->bInterval,
                    endpoint->bSynchAddress);
            }
            retval=-ENODEV;
            goto fault;
        }
    }
    usb_set_intfdata(interface, dev);
    if ((retval=usb_register_dev(interface, &imon512e_class_driver))) {
        IMONERR("can't to get minor for this device");
        goto fault;
    }
    if ( (retval=create_proc_node()) ) {
        IMONERR("could not create /proc/imon512e");
        goto fault;
    }
    dev_minor=interface->minor;
    IMONINF("plugged device: VID: %x, PID: %x, minor %d", device_id->idVendor, 
        device_id->idProduct, interface->minor);
    return 0;
fault:
    if (dev) {
        kref_put(&dev->stuff_kref, delete_stuff_usb_imon512e);
    }
    return retval;
}
/*
 * calls, when device is releaising
 */
static void imon512e_disconnect (struct usb_interface *interface) {
    struct stuff_usb_imon512e *dev;
    int minor=interface->minor;
//    lock_kernel();
    dev=usb_get_intfdata(interface);
    mutex_lock(&dev->stuff_io_mutex);
    usb_set_intfdata(interface, NULL);
    usb_deregister_dev(interface, &imon512e_class_driver); 
    dev->stuff_interface=NULL;
    remove_proc_node();
    mutex_unlock(&dev->stuff_io_mutex);
//    unlock_kernel();
    kref_put(&dev->stuff_kref, delete_stuff_usb_imon512e);
    IMONINF("device imon512e2 with minor #%d disconnected now", minor);
}

static struct usb_driver imon512e_driver = {
    .name =        DRIVER_NAME_P,
    .probe =    imon512e_probe,
    .disconnect =    imon512e_disconnect,
    .id_table =    id_table,
    .no_dynamic_id = 1,
};

static struct file_operations proc_file_ops = {
    .owner = THIS_MODULE,
    .open =     proc_open,
    .read =        proc_read,
    .write =    proc_write,
    .release =    proc_release
};

static int __init imon512e_init (void) {
    int retval = usb_register(&imon512e_driver);
    if (retval) {
        IMONERR("retval=%d can't load spec_driver\n", retval);
        return -retval;
    }
    IMONINF(DRIVER_DESC " " DRIVER_VERSION " loaded");
    return 0;
}

static void __exit imon512e_exit (void) {
    usb_deregister(&imon512e_driver);
    IMONINF("imon512e driver unload\n");
}

module_init(imon512e_init);
module_exit(imon512e_exit);

MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_VERSION(DRIVER_VERSION);
MODULE_AUTHOR(DRIVER_AUTOR);
MODULE_LICENSE("Dual BSD/GPL");

